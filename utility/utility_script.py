from model.account_model import Account, HouseKeeping


def load_accounts():
    HouseKeeping.clean_up()
    accounts = [
        Account("John", "Doe", "123462513", "checking", True, "BofA", "34251"),
        Account("Berlin", "Xref", "84627164832", "saving", True, "Wells Fargo", "453234")
    ]
    print("Inserting records ......")
    Account.objects.bulk_create(accounts)

