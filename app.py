from flask import Flask, render_template

from utility.utility_script import load_accounts

app = Flask(__name__)
load_accounts()


@app.route('/')
def home():
    return render_template('index.html')


@app.route('/hello-world')
def hello_world():
    return 'Hello World!'


if __name__ == '__main__':
    app.run()

