import os
from configparser import ConfigParser


class Util:
    __config_name = None

    @classmethod
    def get_db_info(cls, config_name="DEFAULT"):
        """
        This method return db url and alias name
        :return:
        """
        cls.__config_name = config_name
        config = ConfigParser()
        config_file = os.path.join(os.path.dirname(__file__), 'db_config.cfg')
        config.read(config_file)
        print(config)
        url = config[cls.__config_name]["DataSourceDriver"] + "://"
        url = url + config[cls.__config_name]["Host"] + ":"
        url = url + config[cls.__config_name]["Port"] + "/"
        url = url + config[cls.__config_name]["DbName"]

        print(url)
        print(config[cls.__config_name]["Alias"])
        return url, config[cls.__config_name]["Alias"]



