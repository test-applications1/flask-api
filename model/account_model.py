from pymongo import MongoClient
from pymongo.write_concern import WriteConcern
from pymodm import MongoModel, fields, connect

from config.util import Util

mongodb_uri, db_alias = Util.get_db_info()
connect(mongodb_uri, alias=db_alias)


class Account(MongoModel):

    first_name = fields.CharField(blank=False, required=True)
    last_name = fields.CharField(blank=False, required=True)
    acc_no = fields.CharField(blank=False, required=True)
    acc_type = fields.CharField(blank=False, required=True, default="checking")
    is_active = fields.BooleanField(default=True)
    bank_name = fields.CharField(blank=False, required=True)
    routing_number = fields.CharField(blank=False, required=True, min_length=5, max_length=5)

    class Meta:
        write_concern = WriteConcern(j=True)
        connection_alias = db_alias


class HouseKeeping:

    @staticmethod
    def clean_up():
        mongo_client = MongoClient(mongodb_uri)
        db_list = mongo_client.database_names()
        print(db_list)
        if db_alias in db_list:
            print("deleting "+db_alias)
            mongo_client.drop_database(db_alias)





